# Fart of the Ugue

A set of 12 fugues on ‘fictional language’ songs.

| # | Key | Subject | # voices |
|--:|-----|---------|---------:|
| 1 | G♯ minor | MKSR – <cite>the stream</cite> | 4 |
| 2 | A minor | Tino.S3 – <cite lang="zh-Hans">斯莱特林</cite> | 4 |
| 3 | B♭ minor | <span lang="ja">すこやか大聖堂</span> – <cite lang="ja">声は祈りと成り果てる</cite> | 3 |
| 4 | B minor | <span lang="ja">ハァト民</span> – <cite>ennui</cite> | 5 |
| 5 | C minor | ??? |
| 6 | C♯ minor | ??? |
| 7 | D minor | ??? |
| 8 | E♭ minor | ??? |
| 9 | E minor | ??? |
| 10 | F minor | ??? |
| 11 | F♯ minor | ??? |
| 12 | G minor | ??? |
